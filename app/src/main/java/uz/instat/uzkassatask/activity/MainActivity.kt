package uz.instat.uzkassatask.activity

import android.os.Handler
import android.os.Looper
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import uz.instat.uzkassatask.R
import uz.instat.uzkassatask.base.BaseActivity
import uz.instat.uzkassatask.databinding.ActivityMainBinding
import uz.instat.uzkassatask.helper.logd
import uz.instat.uzkassatask.splash.SplashFragmentDirections

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {
    override fun setupItems() {
        navController = findNavController(R.id.nav_host_fragment_content_main)

//        Handler(Looper.getMainLooper()).postDelayed({
//            navController?.navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
//
//        }, 2000)
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }
}