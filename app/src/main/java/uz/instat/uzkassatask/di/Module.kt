package uz.instat.uzkassatask.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.instat.uzkassatask.network.data_source.NewsApiDataSource
import uz.instat.uzkassatask.network.data_source.NewsApiDataSourceImpl
import uz.instat.uzkassatask.network.repository.NewsApiRepository
import uz.instat.uzkassatask.network.repository.NewsApiRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class Module {


    @Singleton
    @Binds
    abstract fun bindNewsApiDataSource(newsApiDataSourceImpl: NewsApiDataSourceImpl): NewsApiDataSource

    @Singleton
    @Binds
    abstract fun bindNewsApiRepository(newsApiRepositoryImpl: NewsApiRepositoryImpl): NewsApiRepository

}