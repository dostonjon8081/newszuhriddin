package uz.instat.uzkassatask.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.softdata.dyhxx.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import uz.instat.uzkassatask.R
import uz.instat.uzkassatask.databinding.FragmentDetailBinding


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding>(FragmentDetailBinding::inflate) {

    private val args: DetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val arg = args.detailArg
        if (arg != null) {
            binding.apply {
                detailFragmentAuthor.text = arg.author
                detailFragmentTitle.text = arg.title
                detailFragmentPublishedAt.text = arg.publishedAt
                detailFragmentDescription.text = arg.description

                Glide
                    .with(requireContext())
                    .load(arg.urlToImage)
                    .placeholder(R.drawable.ic_no_image)
                    .into(detailFragmentImage)
            }
        }
    }
}