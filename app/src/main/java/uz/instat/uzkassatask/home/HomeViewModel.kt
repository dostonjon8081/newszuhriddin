package uz.instat.uzkassatask.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import uz.instat.uzkassatask.network.NewsResponseModel
import uz.instat.uzkassatask.network.Event
import uz.instat.uzkassatask.network.NetworkResult
import uz.instat.uzkassatask.network.repository.NewsApiRepository
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    application: Application,
    private val repository: NewsApiRepository
) : AndroidViewModel(application) {

    private val _responseNewsApi: MutableLiveData<Event<NetworkResult<NewsResponseModel>>> =
        MutableLiveData()
    val responseNewsApi: LiveData<Event<NetworkResult<NewsResponseModel>>> = _responseNewsApi
    fun getNews(type: String, key: String) = viewModelScope.launch {
        repository.getNews(type, key).collect { values ->
            _responseNewsApi.postValue(Event(values))
        }
    }
}