package uz.instat.uzkassatask.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.instat.uzkassatask.R
import uz.instat.uzkassatask.network.Articles

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.VH>() {

    private val list = mutableListOf<Articles>()
    private var clickRV: ClickRVItem? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }

    fun rvClickListener(clickRVItem: ClickRVItem) {
        this.clickRV = clickRVItem
    }

    fun submitList(list: List<Articles>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount() = list.size

    inner class VH(private val view: View) : RecyclerView.ViewHolder(view) {

        private val author: TextView = view.findViewById(R.id.news_item_author)
        private val title: TextView = view.findViewById(R.id.news_item_title)
        private val publishedAt: TextView = view.findViewById(R.id.news_item_published_at)
        private val image: ImageView = view.findViewById(R.id.news_item_image)


        fun onBind(articles: Articles) {
            author.text = articles.author
            title.text = articles.title
            publishedAt.text = articles.publishedAt

            Glide
                .with(image.context)
                .load(articles.urlToImage)
                .centerCrop()
                .placeholder(R.drawable.ic_no_image)
                .into(image)

            view.setOnClickListener { clickRV?.itemClick(articles) }
        }

    }

}
