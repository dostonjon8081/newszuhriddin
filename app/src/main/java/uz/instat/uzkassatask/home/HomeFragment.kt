package uz.instat.uzkassatask.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.softdata.dyhxx.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import uz.instat.uzkassatask.databinding.FragmentHomeBinding
import uz.instat.uzkassatask.helper.KEY
import uz.instat.uzkassatask.helper.isOnline
import uz.instat.uzkassatask.helper.logd
import uz.instat.uzkassatask.network.Articles
import uz.instat.uzkassatask.network.EventObserver
import uz.instat.uzkassatask.network.NetworkResult

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate), ClickRVItem {

    private val viewModel: HomeViewModel by viewModels()
    private val newsAdapter: NewsAdapter by lazy(LazyThreadSafetyMode.NONE) { NewsAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isOnline(requireContext())) {
            binding.wp7progressBar.showProgressBar()
            viewModel.getNews("*", KEY)

            viewModel.responseNewsApi.observe(viewLifecycleOwner, EventObserver {

                when (it) {
                    is NetworkResult.Error -> {
                        logd("error")
                    }
                    is NetworkResult.Loading -> {
                        logd("loading")
                    }
                    is NetworkResult.Success -> {
                        newsAdapter.submitList(it.data!!.articles)
                        binding.homeFragmentRv.adapter = newsAdapter
                        newsAdapter.rvClickListener(this)
                        binding.wp7progressBar.hideProgressBar()
                    }
                }

            })
        } else Toast.makeText(requireContext(), "No Connect Network", Toast.LENGTH_LONG).show()
    }

    override fun itemClick(articles: Articles) {
        getBaseActivity {
            it.navController?.navigate(
                HomeFragmentDirections.actionHomeFragmentToDetailFragment(
                    articles
                )
            )
        }
    }
}