package uz.instat.uzkassatask.home

import uz.instat.uzkassatask.network.Articles

interface ClickRVItem {
    fun itemClick(articles: Articles)
}
