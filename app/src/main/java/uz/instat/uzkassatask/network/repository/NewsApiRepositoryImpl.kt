package uz.instat.uzkassatask.network.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.instat.uzkassatask.network.NewsResponseModel
import uz.instat.uzkassatask.network.NetworkResult
import uz.instat.uzkassatask.network.data_source.NewsApiDataSource
import javax.inject.Inject

class NewsApiRepositoryImpl @Inject constructor(private val source: NewsApiDataSource) :
    NewsApiRepository, BaseApiResponse() {
    override fun getNews(type: String, key: String): Flow<NetworkResult<NewsResponseModel>> {
        return flow {
            emit(safeApiCall {
                source.getNews(type, key)
            })
        }.flowOn(Dispatchers.IO)
    }
}