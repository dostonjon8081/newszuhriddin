package uz.instat.uzkassatask.network.data_source

import retrofit2.Response
import uz.instat.uzkassatask.network.NewsResponseModel

interface NewsApiDataSource {
    suspend fun getNews(type:String,key:String):Response<NewsResponseModel>
}