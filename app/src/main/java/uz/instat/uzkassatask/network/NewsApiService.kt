package uz.instat.uzkassatask.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.instat.uzkassatask.helper.KEY

interface NewsApiService {

    @GET("v2/everything")
    suspend fun getNews(
        @Query("q") type: String,
        @Query("apiKey") key: String = KEY
    ): Response<NewsResponseModel>
}