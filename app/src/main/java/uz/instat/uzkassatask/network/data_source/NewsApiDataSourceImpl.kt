package uz.instat.uzkassatask.network.data_source

import retrofit2.Response
import uz.instat.uzkassatask.network.NewsResponseModel
import uz.instat.uzkassatask.network.NewsApiService
import javax.inject.Inject

class NewsApiDataSourceImpl @Inject constructor(private val apiService: NewsApiService) :
    NewsApiDataSource {
    override suspend fun getNews(type: String, key: String): Response<NewsResponseModel> {
        return apiService.getNews(type, key)
    }
}