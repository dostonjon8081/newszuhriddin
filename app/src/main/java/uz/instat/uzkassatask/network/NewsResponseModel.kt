package uz.instat.uzkassatask.network

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsResponseModel(
    @SerializedName("status")
    val status: String,
    @SerializedName("totalResults")
    val totalResults: String,
    @SerializedName("articles")
    val articles: List<Articles>,

    ):Parcelable

@Parcelize
class Articles(
    @SerializedName("source")
    val source: Source,
    @SerializedName("author")
    val author: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("urlToImage")
    val urlToImage: String,
    @SerializedName("publishedAt")
    val publishedAt: String,
    @SerializedName("content")
    val content: String
) : Parcelable

@Parcelize
class Source(
    @SerializedName("di")
    val id: String,
    @SerializedName("name")
    val name: String
) : Parcelable