package uz.instat.uzkassatask.network.repository

import kotlinx.coroutines.flow.Flow
import uz.instat.uzkassatask.network.NewsResponseModel
import uz.instat.uzkassatask.network.NetworkResult

interface NewsApiRepository {
    fun getNews(type: String, key: String): Flow<NetworkResult<NewsResponseModel>>
}